#!/usr/bin/perl

use strict;
use warnings;
use Mojo::SQLite;


my $sql = Mojo::SQLite->new('sqlite:queue.db');
$sql->migrations->name('rover')->from_string(<<EOF)->migrate;
-- 1 up
create table queue (id integer primary key autoincrement, command varchar(1));
-- 1 down
drop table queue;
EOF

$sql->migrations->migrate(0)->migrate;