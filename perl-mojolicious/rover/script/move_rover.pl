#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';
use utf8;

use Mojo::Base 'Mojolicious::Controller', -signatures;
use feature 'say';

use Mojo::SQLite;
use Mojo::UserAgent;

use MarsRover;
use Transport;

my $sql = Mojo::SQLite->new('sqlite:/usr/src/app/queue.db');
my $results = $sql->db->query("select * from queue order by id");
my $rover = MarsRover->new(transport => Transport->new);

while (my $action = $results->hash) {
    say("Next command: $action->{id}  $action->{command}");
    my $result = $rover->make($action->{command});
    if (exists($result->{result}) && $result->{result} eq 'Команда выполнена успешно!') {
        $sql->db->delete('queue', {id => $action->{id}});
    }
}