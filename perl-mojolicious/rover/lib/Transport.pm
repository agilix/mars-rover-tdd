package Transport;

use Mojo::Base -base, -signatures;
use Mojo::UserAgent;
use Mojo::Exception qw(check);

package Transport::URLNotExists {
    use Mojo::Base 'Mojo::Exception';
}


sub new($ref) {
    return bless { ua => Mojo::UserAgent->new }, $ref
}

sub move($self) {
    $self->_send('move');
}

sub right($self) {
    $self->_send('right');
}


sub left($self) {
    $self->_send('left');
}

sub _send($self, $path) {
    my $retries = 5;
    my $result;

    while ($retries > 0) {
        $result = $self->_request($path);
        last if !exists($result->{code});
        last if exists($result->{code}) && $result->{code} eq '404';
        $self->delay(1);
        $retries--;
        say "Error: $result->{code} tries left: $retries";
    }

    return $result;
}

sub delay($self, $duration) {
    sleep $duration;
}

sub _request($self, $path) {
    my $url = "http://rover:3000/$path";
    say "try to get: $url";
    my $tx = $self->{ua}->get($url);
    if ($tx->res->is_success) {
        say 'OK!';
        return $tx->res->json
    }
    else {
        my $code = $tx->res->code || -1;
        say "Error: $code";
        return { code => $code }
    }
}

1;