package rover::Controller::Commands;

use Mojo::Base 'Mojolicious::Controller', -signatures;
use Mojo::Exception qw(check);
use Mojo::SQLite;

use MarsRover;

our $POINT = MarsRover::point(0, 0, 'N');
our $GRID = { x => 10, y => 10 };

sub post($self) {
    my $home = $self->app->home->detect;

    my $rover = MarsRover->new(
        position => $POINT,
        grid     => $GRID,
        db       => Mojo::SQLite->new("sqlite:$home/queue.db")->db()
    );

    my $result = undef;
    eval {
        $result = $rover->move($self->req->json->{command}, $POINT);
    };

    check $@ => [
        'MarsRover::BoundaryException' => sub {$result->{error} = "unable to cross grid boundary"}
    ];

    $self->render(
        json   => $result,
        status => exists($result->{error}) ? 400 : 201
    );
}

sub map($self) {
    my $point = $self->req->json->{position};

    $POINT = MarsRover::point($point->{x}, $point->{y}, $point->{direction});
    $GRID = $self->req->json->{grid};

    $self->render(
        json   => { 'status' => 'ok' },
        status => 201
    );
}

1;