package rover::Controller::Coffee;
use Mojo::Base 'Mojolicious::Controller', -signatures;

sub brew ($self) {
  $self->render(text => 'coffee ready!', status => 418);
}

1;
