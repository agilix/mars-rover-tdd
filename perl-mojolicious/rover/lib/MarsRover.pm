package MarsRover;

use Mojo::Base -base, -signatures;
use Mojo::SQLite;

package MarsRover::BoundaryException {
    use Mojo::Base 'Mojo::Exception';
}

sub point($x, $y, $direction) {
    return { 'point' => { 'x' => $x, y => $y, 'direction' => $direction } }
}

sub new($ref, %args) {
    return bless \%args, $ref
}

sub move($self, $commands, $position) {
    foreach my $command (split //, $commands) {
        if ($command eq 'R') {
            if ($position->{point}{direction} eq 'N') {
                $position->{point}{direction} = 'E';
            }
            elsif ($position->{point}{direction} eq 'E') {
                $position->{point}{direction} = 'S';
            }
            elsif ($position->{point}{direction} eq 'S') {
                $position->{point}{direction} = 'W';
            }
            elsif ($position->{point}{direction} eq 'W') {
                $position->{point}{direction} = 'N';
            }
        }

        if ($command eq 'L') {
            if ($position->{point}{direction} eq 'N') {
                $position->{point}{direction} = 'W';
            }
            elsif ($position->{point}{direction} eq 'W') {
                $position->{point}{direction} = 'S';
            }
            elsif ($position->{point}{direction} eq 'S') {
                $position->{point}{direction} = 'E';
            }
            elsif ($position->{point}{direction} eq 'E') {
                $position->{point}{direction} = 'N';
            }
        }

        if ($command eq 'M' ) {
            if($position->{point}{direction} eq 'N') {
                ++$position->{point}{y};
            } elsif ($position->{point}{direction} eq 'S') {
                --$position->{point}{y};
            } elsif ($position->{point}{direction} eq 'E') {
                ++$position->{point}{x};
            } elsif ($position->{point}{direction} eq 'W') {
                --$position->{point}{x};
            }
        }
        $self->validate_position($position);
    }

    foreach my $command (split //, $commands) {
        $self->{db}->insert('queue', { command => $command });
    }

    return $position;
}

sub validate_position($self, $position) {

    if ($position->{point}{y} < 0) {
        MarsRover::BoundaryException->throw("bottom bound crossed")
    }

    if ($position->{point}{x} < 0) {
        MarsRover::BoundaryException->throw("left bound crossed")
    }

    if ($position->{point}{y} > $self->{grid}{y}) {
        MarsRover::BoundaryException->throw("top bound crossed")
    }

    if ($position->{point}{x} > $self->{grid}{x}) {
        MarsRover::BoundaryException->throw("top bound crossed")
    }

}

sub make($self, $action) {
    if ($action eq 'M') {
        return $self->{transport}->move();
    }

    if ($action eq 'R') {
        return $self->{transport}->right();
    }

    if ($action eq 'L') {
        return $self->{transport}->left();
    }

}

1;