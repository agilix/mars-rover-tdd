use strict;
use warnings FATAL => 'all';
use feature 'signatures';
no warnings 'experimental';

use Test::More;
use Test::Spec;
use Test::Exception;

use MarsRover;

sub grid {
    my ($x, $y) = @_;
    return {
        'x' => $x,
        'y' => $y
    }
}

{
    package FakeDB;

    sub new {
        my ($ref, %args) = @_;
        return bless \%args, $ref
    }

    sub insert {

    }

    1;
}

describe "validate_position() should" => sub {

    it "throw exception when position out of grid on bottom" => sub {
        my $rover = MarsRover->new(
            position => MarsRover::point(0, 0, 'S'),
            grid     => grid(10, 10),
            db       => FakeDB->new(),
        );

        throws_ok(sub {
            $rover->move('M', MarsRover::point(0, 0, 'S'));
        }, 'MarsRover::BoundaryException');

    };

    it "throw exception when position out of grid on left" => sub {
        my $rover = MarsRover->new(
            position => MarsRover::point(0, 0, 'W'),
            grid     => grid(10, 10),
            db       => FakeDB->new(),
        );

        throws_ok(sub {
            $rover->move('M', MarsRover::point(0, 0, 'W'));
        }, 'MarsRover::BoundaryException');

    };

    it "throw exception when position out of grid on top" => sub {

        my $rover = MarsRover->new(
            position => MarsRover::point(0, 0, 'N'),
            grid     => grid(2, 2),
            db       => FakeDB->new(),
        );

        throws_ok(sub {
            $rover->move('M', MarsRover::point(0, 2, 'N'));
        }, 'MarsRover::BoundaryException');
    };

    it "throw exception when position out of grid on top" => sub {

        my $rover = MarsRover->new(
            position => MarsRover::point(0, 0, 'N'),
            grid     => grid(2, 2),
            db       => FakeDB->new(),
        );

        throws_ok(sub {
            $rover->move('M', MarsRover::point(2, 0, 'E'));
        }, 'MarsRover::BoundaryException');
    };

};

runtests();