use strict;
use warnings FATAL => 'all';
use utf8;

use Test::More;
use Test::Spec;

use Transport;
{

    package NotFoundTransport;

    use Mojo::Base -base, -signatures;
    use base 'Transport';

    sub _request($self, $path) {
        return {
            code => 404
        }
    }

    package ConnectionErrorTransport;

    use Mojo::Base -base, -signatures;
    use base 'Transport';

    sub _request($self, $path) {
        $self->{_request}{times}++;
        --$self->{times};

        if ($self->{times} > 0) {
            return {
                code  => -1,
                error => 'connection'
            }
        }

        return {
            result => "Команда выполнена успешно!"
        }
    }
}

describe "Transport should" => sub {
    it "stop on 404" => sub {
        my $transport = NotFoundTransport->new;

        my $res = $transport->_send("404");

        is $res->{code}, 404;
    };

    it "retry on error" => sub {
        my $transport = ConnectionErrorTransport->new(times => 2);

        my $res = $transport->_send("move");

        is $transport->{_request}{times}, 2;

        is $res->{result}, 'Команда выполнена успешно!';
    };
};

runtests();