use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('rover');
$t->post_ok('/brew_coffee')->status_is(418)->content_like(qr/coffee ready!/i);

$t->post_ok('/map' => json => {
    grid     => { x => 10, y => 10 },
    position => { x => 0, y => 0, direction => 'N' }
})->status_is(201);

$t->post_ok('/commands' => json => { command => 'M' })->status_is(201)->json_is({ 'point' => { 'x' => 0, 'y' => 1, 'direction' => 'N' } });

$t->post_ok('/map' => json => {
    grid     => { x => 10, y => 10 },
    position => { x => 0, y => 0, direction => 'N' }
})->status_is(201);

$t->post_ok('/commands' => json => { command => 'MM' })->status_is(201)->json_is({ 'point' => { 'x' => 0, 'y' => 2, 'direction' => 'N' } });


# $t->post_ok('/map' => json => {
#     'position' => { 'x' => 0, 'y' => 0, 'direction' => "N" },
#     'grid'     => { 'x' => 10, 'y' => 10 } }
# )->status_is(201);

done_testing();
