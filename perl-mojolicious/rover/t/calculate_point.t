use strict;
use warnings;

use Test::More;
use Test::Spec;
use Test::Exception;

use rover::Controller::Commands;
use MarsRover;

sub grid {
    my ($x, $y) = @_;
    return {
        'x' => $x,
        'y' => $y
    }
}

{
    package FakeDB;

    sub new {
        my ($ref, %args) = @_;
        return bless \%args, $ref
    }

    sub insert {

    }

    1;
}

describe "Rover should" => sub {
    my $rover;
    before sub {
        $rover = MarsRover->new(
            position => MarsRover::point(0, 0, 'N'),
            grid     => grid(10, 10),
            db       => FakeDB->new(),
        );
    };

    it "on M do one step to North" => sub {
        my $result = $rover->move('M', MarsRover::point(0, 0, 'N'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 1;
        is $result->{point}{direction}, 'N';
    };

    it "on M do one step to North" => sub {
        my $result = $rover->move('M', MarsRover::point(0, 0, 'E'));

        is $result->{point}{x}, 1;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'E';
    };

    it "on M do one step to North" => sub {
        my $result = $rover->move('M', MarsRover::point(0, 1, 'S'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'S';
    };

    it "on M do one step to North" => sub {
        my $result = $rover->move('M', MarsRover::point(1, 0, 'W'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'W';
    };

    it "on MM do two steps to North" => sub {
        my $result = $rover->move('MM', MarsRover::point(0, 0, 'N'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 2;
        is $result->{point}{direction}, 'N';
    };

    it "when rover points on North on R do turn on East" => sub {
        my $result = $rover->move('R', MarsRover::point(0, 0, 'N'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'E';
    };

    it "when rover points on Earth on R do turn on South" => sub {
        my $result = $rover->move('R', MarsRover::point(0, 0, 'E'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'S';
    };

    it "when rover points on South on R do turn on West" => sub {
        my $result = $rover->move('R', MarsRover::point(0, 0, 'S'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'W';
    };

    it "when rover points on South on R do turn on West" => sub {
        my $result = $rover->move('R', MarsRover::point(0, 0, 'W'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'N';
    };

    it "when rover points on North on R do turn on East" => sub {
        my $result = $rover->move('L', MarsRover::point(0, 0, 'N'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'W';
    };

    it "when rover points on West on R do turn on South" => sub {
        my $result = $rover->move('L', MarsRover::point(0, 0, 'W'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'S';
    };

    it "when rover points on South on R do turn on East" => sub {
        my $result = $rover->move('L', MarsRover::point(0, 0, 'S'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'E';
    };

    it "when rover points on East on R do turn on North" => sub {
        my $result = $rover->move('L', MarsRover::point(0, 0, 'E'));

        is $result->{point}{x}, 0;
        is $result->{point}{y}, 0;
        is $result->{point}{direction}, 'N';
    };

};

runtests();