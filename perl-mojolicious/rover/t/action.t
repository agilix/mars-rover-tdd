use strict;
use warnings FATAL => 'all';

use Test::More;
use Test::Spec;

use MarsRover;
use Transport;

{
    package FakeTransport;

    use Mojo::Base -base, -signatures;
    use base 'Transport';

    sub _request($self, $path) {
        $self->{_request}{called} = 1;
        $self->{_request}{path} = $path;

        return {}
    }

    1;
}

describe "Rover should" => sub {
    my $rover;
    my $transport;
    before sub {
        $transport = FakeTransport->new;
        $rover = MarsRover->new(transport => $transport);
    };

    it "on M do one step" => sub {
        $rover->make('M');

        is $transport->{_request}{called}, 1;
        is $transport->{_request}{path}, 'move';
    };

    it "on R do right turn" => sub {
        $rover->make('R');

        is $transport->{_request}{called}, 1;
        is $transport->{_request}{path}, 'right';
    };
};

runtests();