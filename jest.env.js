expect.extend({
  toHaveEvent: (received, eventName) => {
    if (received.includes(eventName)) {
      return {
        message: () => `Event with name "${eventName}" exists`,
        pass: true,
      };
    }

    return {
      message: () => `Event with name "${eventName}" does not exist`,
      pass: false,
    };
  },

  toHaveEventOnce: (received, eventName) => {
    const count = received.filter((e) => e === eventName).length;
    if (count === 1) {
      return {
        message: () => `Event with name "${eventName}" exists once`,
        pass: true,
      };
    }

    return {
      message: () => `Event with name "${eventName}" exist ${count} times`,
      pass: false,
    };
  },
});
