/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  modulePathIgnorePatterns: ["__tests__", "spec"],
  setupFilesAfterEnv: ["<rootDir>/jest.env.js"],
};
