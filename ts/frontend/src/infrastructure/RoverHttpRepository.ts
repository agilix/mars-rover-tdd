import type { Grid } from "rover";
import type { RoverRepository } from "../application/types";

export class RoverHttpRepository implements RoverRepository {
  async getRoutesList() {
    const result = await client.get("api/commands");
    return result.routes;
  }

  async addRoute(route: string) {
    const result = await client.post("api/commands", { command: route });
    return { position: result.point };
  }

  getGrid() {
    return client.get("api/map");
  }

  setGrid(grid: Grid) {
    return client.post("api/map", { grid });
  }
}

class Client {
  async get(url: string) {
    return this.jsonResponse(await fetch(url));
  }

  async post(url: string, body: unknown) {
    return this.jsonResponse(
      await fetch(url, {
        method: "POST",
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" },
      })
    );
  }

  jsonResponse(response: Response) {
    if (!response.ok)
      throw new Error(`"${response.url}" request error: ${response.status}`);

    return response.json();
  }
}

const client = new Client();
