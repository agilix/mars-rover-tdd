import type { Command } from "rover";

export class RoutesInMemoryRepository {
  constructor(public commands: Command[]) {}

  async getRoutesList() {
    return this.commands.slice();
  }

  async addRoute(route: string) {
    return { position: { x: 0, y: 0, direction: "N" as const } };
  }
}
