import type { Grid, Point } from "rover";
import type { RoverRepository } from "../application/types";

export class RoverInMemoryRepository implements RoverRepository {
  constructor(public commands: string[]) {}

  async getRoutesList() {
    return this.commands.slice();
  }

  async addRoute() {
    return { position: { x: 0, y: 0, direction: "N" as const } };
  }

  async getGrid() {
    return {
      grid: { x: 10, y: 10 },
      position: { x: 0, y: 0, direction: "N" as const },
    };
  }

  async setGrid(grid: Grid) {
    throw new Error("not implemented");
  }
}
