import { reactive } from "vue";
import { roverApplication } from "./main";

export const store = reactive({
  ...roverApplication.getState(),
  loadRoutes: () => roverApplication.routes.loadRoutes(),
  addRoute: (command: string) => roverApplication.addRoute(command),
  loadGridAndPosition: () => roverApplication.map.loadGridAndPosition(),
  setGrid: (x: number, y: number) => roverApplication.setGrid(x, y),
});

roverApplication.subscribe(() => {
  Object.assign(store, roverApplication.getState());
});
