import { RoutesApplication } from "./routes";
import { routeForCommands, validateRouteInput } from "rover";
import { RoverHttpRepository } from "../infrastructure/RoverHttpRepository";
import { MapApplication } from "@/application/map";
import { ApplicationWithEvents } from "@/application/base";

const repository = new RoverHttpRepository();

class RoverApplication extends ApplicationWithEvents {
  routes: RoutesApplication;
  map: MapApplication;
  private addRouteError: string | null = null;

  constructor() {
    super();
    this.routes = new RoutesApplication(repository, this.emit);
    this.map = new MapApplication(repository, this.emit);
  }

  getState() {
    const routes = this.routes.getRoutesList();
    const points = routeForCommands(routes || []);
    return {
      addRouteError: this.addRouteError,
      grid: this.map.getGrid(),
      position: this.map.getPosition(),
      points,
      routes,
    };
  }

  async addRoute(input: string) {
    this.addRouteError = validateRouteInput(input);

    if (this.addRouteError) return this.emit("add route error");

    const result = await this.routes.addRoute(input);
    if (!result) return;

    this.map.updatePosition(result.position);
  }

  async setGrid(x: number, y: number) {
    await this.map.setGrid(x, y);
    await this.routes.loadRoutes(true);
    this.emit("grid reset");
  }
}

export const roverApplication = new RoverApplication();
