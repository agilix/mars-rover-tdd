import { RoutesApplication } from "./routes";
import { RoutesInMemoryRepository } from "../infrastructure/RoutesInMemoryRepository";
import type { Command } from "rover";

describe("getCommandsList", () => {
  it("returns null while loading data", () => {
    const { app } = makeApplication();

    const commands = app.getRoutesList();

    expect(commands).toEqual(null);
  });

  it("notifies interested parties when data loading started", async () => {
    const { app, getEvents } = makeApplication();

    app.loadRoutes();

    expect(await getEvents()).toHaveEvent("commands loading");
  });

  it("does not load commands again while loading", async () => {
    const { app, getEvents } = makeApplication();

    app.loadRoutes();
    app.loadRoutes();

    expect(await getEvents()).toHaveEventOnce("commands loading");
  });

  it("notifies interested parties when data loaded", async () => {
    const { app, getEvents } = makeApplication();

    await app.loadRoutes();

    expect(await getEvents()).toHaveEvent("commands loaded");
  });

  it("returns data from repository when loaded", async () => {
    const { app } = makeApplication(["M", "L"]);

    await app.loadRoutes();

    expect(app.getRoutesList()).toEqual(["M", "L"]);
  });
});

describe("addCommand", () => {
  it("add command to list", async () => {
    const { app } = makeApplication();
    await app.loadRoutes();

    app.addRoute("M");

    expect(app.getRoutesList()).toEqual(["M"]);
  });

  it("saves command to repository", async () => {
    const { app, repository } = makeApplication();
    await app.loadRoutes();
    repository.addRoute = jest.fn();

    app.addRoute("M");

    expect(repository.addRoute).toHaveBeenCalledWith("M");
  });
});

const makeApplication = (data: Command[] = []) => {
  const events: string[] = [];

  const repository = new RoutesInMemoryRepository(data);
  const app = new RoutesApplication(repository, (eventName) => {
    events.push(eventName);
  });

  const act = async (callback: () => void) => {
    await callback();
    await new Promise(process.nextTick);
  };

  const waitForEvents = async () => await new Promise(process.nextTick);

  const getEvents = async () => {
    await new Promise(process.nextTick);
    return events;
  };

  return { app, act, getEvents, repository, waitForEvents };
};

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace jest {
    interface Matchers<R> {
      toHaveEvent(eventName: string): R;
      toHaveEventOnce(eventName: string): R;
    }
  }
}
