import type { Command } from "rover";
import type { RoverRepository, Listener } from "./types";

export class RoutesApplication {
  private routes: Command[] | null = null;
  private routesLoadStarted = false;

  constructor(
    private repository: RoverRepository,
    private listener: Listener
  ) {}

  getRoutesList() {
    return this.routes;
  }

  async addRoute(input: string) {
    if (!this.routes) throw new Error("please load routes before adding new");

    const route = input.split("") as Command[];

    const routes = this.routes;
    this.routes = [...routes, ...route];
    this.listener("command sent to server");
    try {
      const result = await this.repository.addRoute(input);
      this.listener("command added");
      return result;
    } catch (error) {
      this.routes = routes;
      this.listener("add command error");
      throw error;
    }
  }

  async loadRoutes(reload = false) {
    if (!reload && this.routesLoadStarted) return;

    this.routesLoadStarted = true;
    this.listener("commands loading");

    this.routes = await this.repository.getRoutesList();
    this.listener("commands loaded");
  }
}
