import type { Point } from "rover";

export const isPassedPoint = (points: Point[], x: number, y: number) =>
  points.some((p) => p.x === x && p.y === y);
