import type { Listener } from "./types";

export class ApplicationWithEvents {
  private listeners: Listener[] = [];

  subscribe = (listener: Listener) => {
    this.listeners.push(listener);
    return () => {
      this.listeners = this.listeners.filter((l) => l !== listener);
    };
  };

  emit = (event: string) => {
    console.log(event);
    this.listeners.forEach((l) => l(event));
  };
}
