import type { Grid, Point } from "rover";
import type { RoverRepository, Listener } from "./types";

export class MapApplication {
  private grid: Grid | null = null;
  private position: Point | null = null;
  private loadStarted = false;

  constructor(
    private repository: RoverRepository,
    private listener: Listener
  ) {}

  getGrid() {
    return this.grid;
  }

  getPosition() {
    return this.position;
  }

  updatePosition(position: Point) {
    this.position = position;
    this.listener("position updated");
  }

  async setGrid(x: number, y: number) {
    const { grid, position } = await this.repository.setGrid({ x, y });
    this.grid = grid;
    this.position = position;
  }

  async loadGridAndPosition() {
    if (this.loadStarted) return;

    this.loadStarted = true;
    this.listener("map loading");

    const { grid, position } = await this.repository.getGrid();
    this.grid = grid;
    this.position = position;
    this.listener("map ready");
  }
}
