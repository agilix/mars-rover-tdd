import type { Command, Grid, Point } from "rover";

export type Listener = (event: string) => void;

export interface RoverRepository {
  getRoutesList: () => Promise<Command[]>;
  addRoute: (route: string) => Promise<{ position: Point }>;
  getGrid: () => Promise<{ grid: Grid; position: Point }>;
  setGrid: (grid: Grid) => Promise<{ grid: Grid; position: Point }>;
}
