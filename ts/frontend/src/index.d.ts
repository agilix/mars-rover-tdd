export {};

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace jest {
    interface Matchers<R> {
      toHaveEvent(eventName: string): R;
      toHaveEventOnce(eventName: string): R;
    }
  }
}
