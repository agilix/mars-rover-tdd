import { Command, Direction, Point } from "rover";

export function routeForCommands(
  commands: Command[],
  start: Point = { x: 0, y: 0, direction: "N" }
) {
  const result = [start];
  let position = start;
  for (const command of commands) {
    position = nextPosition(command, position);
    result.push(position);
  }
  return result;
}

const nextPosition = (command: Command, position: Point): Point => {
  if (command === "L")
    return { ...position, direction: nextLeftDirection(position.direction) };
  if (command === "R")
    return { ...position, direction: nextRightDirection(position.direction) };
  if (command === "M") return nextPoint(position);
  return position;
};

const nextLeftDirection = (direction: Direction): Direction => {
  if (direction === "N") return "W";
  if (direction === "W") return "S";
  if (direction === "S") return "E";
  if (direction === "E") return "N";
};
const nextRightDirection = (direction: Direction): Direction => {
  if (direction === "N") return "E";
  if (direction === "E") return "S";
  if (direction === "S") return "W";
  if (direction === "W") return "N";
};
const nextPoint = (position: Point): Point => {
  if (position.direction === "N") return { ...position, y: position.y + 1 };
  if (position.direction === "S") return { ...position, y: position.y - 1 };
  if (position.direction === "E") return { ...position, x: position.x + 1 };
  if (position.direction === "W") return { ...position, x: position.x - 1 };
};
