export type Command = "M" | "L" | "R";

export type Direction = "N" | "E" | "S" | "W";

export type Point = { x: number; y: number; direction: Direction };

export type Grid = {
  x: number; // Horizontal size. E.g. 10 means range [0..9] of positions
  y: number; // Vertical size. E.g. 10 means range [0..9] of positions
  obstacles?: Array<{ x: number; y: number }>;
};
