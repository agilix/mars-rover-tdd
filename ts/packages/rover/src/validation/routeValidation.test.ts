import { Point } from "../types";
import { validatePoint } from "./routeValidation";

test.each([
  ["left X", { x: -1, y: 0, direction: "N" as const }],
  ["right X", { x: 10, y: 0, direction: "N" as const }],
  ["top Y", { x: 0, y: 10, direction: "N" as const }],
  ["bottom Y", { x: 0, y: -1, direction: "N" as const }],
])(
  "given point outside %s boundary returns error",
  (desc: string, point: Point) => {
    expect(validatePoint(point, { x: 10, y: 10 })).toEqual(
      "route should not cross boundaries"
    );
  }
);

test("given point inside boundaries returns undefined", () => {
  expect(
    validatePoint({ x: 0, y: 0, direction: "N" }, { x: 10, y: 10 })
  ).toEqual(undefined);
});

test("given point at obstacle returns error", () => {
  expect(
    validatePoint(
      { x: 0, y: 0, direction: "N" },
      {
        x: 1,
        y: 1,
        obstacles: [{ x: 0, y: 0 }],
      }
    )
  ).toEqual("route should not cross obstacles");
});
