import { validateRouteInput } from "./inputValidation";

describe("validateRouteInput", () => {
  it("returns error given empty string", () => {
    expect(validateRouteInput("")).toBe("commands should not be empty");
  });

  it("returns error given unknown character", () => {
    expect(validateRouteInput("u")).toBe(
      "commands should contain only M,R or L symbols"
    );
  });

  it("returns error given unknown character after command", () => {
    expect(validateRouteInput("Mu")).toBe(
      "commands should contain only M,R or L symbols"
    );
  });

  it("returns null given one M", () => {
    expect(validateRouteInput("M")).toBeNull();
  });

  it("returns null given multiple M", () => {
    expect(validateRouteInput("MMM")).toBeNull();
  });

  it("returns null given one L", () => {
    expect(validateRouteInput("L")).toBeNull();
  });

  it("returns null given one R", () => {
    expect(validateRouteInput("R")).toBeNull();
  });

  it("returns null given multiple commands sequence", () => {
    expect(validateRouteInput("MRLLRM")).toBeNull();
  });
});
