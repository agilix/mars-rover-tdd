export const validateRouteInput = (input: string): string | null => {
  try {
    checkEmpty(input);
    checkSymbols(input);
  } catch (error) {
    return String(error);
  }

  return null;
};

const checkEmpty = (input: string) => {
  if (!input) throw "commands should not be empty";
};

const checkSymbols = (input: string) => {
  if (input.split("").some((c) => !"MRL".includes(c)))
    throw "commands should contain only M,R or L symbols";
};
