import { Grid, Point } from "../types";

export function validatePoint(point: Point, grid: Grid): string | undefined {
  const obstacles = grid.obstacles ?? [];

  if (
    point.x < 0 ||
    point.x > grid.x - 1 ||
    point.y < 0 ||
    point.y > grid.y - 1
  )
    return "route should not cross boundaries";

  if (obstacles.some((o) => o.x === point.x && o.y === point.y))
    return "route should not cross obstacles";
}
