import { routeForCommands } from "./router";
import { Point } from "./types";

const defaultStart = { x: 0, y: 0, direction: "N" };

test("returns starting point given no commands", () => {
  expect(routeForCommands([])).toEqual([defaultStart]);
});

test("returns point reached by one move in N direction", () => {
  expect(routeForCommands(["M"])).toEqual([
    defaultStart,
    { x: 0, y: 1, direction: "N" },
  ]);
});

test("returns point reached by move in E direction", () => {
  const start: Point = { ...defaultStart, direction: "E" };
  expect(routeForCommands(["M"], start)).toEqual([
    start,
    { x: 1, y: 0, direction: "E" },
  ]);
});

test("returns point reached by move in W direction", () => {
  const start: Point = { ...defaultStart, direction: "W" };
  expect(routeForCommands(["M"], start)).toEqual([
    start,
    { x: -1, y: 0, direction: "W" },
  ]);
});

test("returns point reached by move in S direction", () => {
  const start: Point = { ...defaultStart, direction: "S" };
  expect(routeForCommands(["M"], start)).toEqual([
    start,
    { x: 0, y: -1, direction: "S" },
  ]);
});

test("returns point reached by two moves in N direction", () => {
  expect(routeForCommands(["M", "M"])).toEqual([
    defaultStart,
    { x: 0, y: 1, direction: "N" },
    { x: 0, y: 2, direction: "N" },
  ]);
});

test("returns point reached by left turn", () => {
  expect(routeForCommands(["L"])).toEqual([
    defaultStart,
    { x: 0, y: 0, direction: "W" },
  ]);
});

test("returns points reached by multiple left turns", () => {
  expect(routeForCommands(["L", "L", "L", "L"])).toEqual([
    defaultStart,
    { x: 0, y: 0, direction: "W" },
    { x: 0, y: 0, direction: "S" },
    { x: 0, y: 0, direction: "E" },
    { x: 0, y: 0, direction: "N" },
  ]);
});

test("returns point reached by right turn", () => {
  expect(routeForCommands(["R"])).toEqual([
    defaultStart,
    { x: 0, y: 0, direction: "E" },
  ]);
});

test("returns points reached by multiple left turns", () => {
  expect(routeForCommands(["R", "R", "R", "R"])).toEqual([
    defaultStart,
    { x: 0, y: 0, direction: "E" },
    { x: 0, y: 0, direction: "S" },
    { x: 0, y: 0, direction: "W" },
    { x: 0, y: 0, direction: "N" },
  ]);
});

test("returns point reached by complex commands list", () => {
  expect(routeForCommands(["M", "R", "M", "L", "M"])).toEqual([
    defaultStart,
    { x: 0, y: 1, direction: "N" },
    { x: 0, y: 1, direction: "E" },
    { x: 1, y: 1, direction: "E" },
    { x: 1, y: 1, direction: "N" },
    { x: 1, y: 2, direction: "N" },
  ]);
});
