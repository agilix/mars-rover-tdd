import express from "express";
import bodyParser from "body-parser";
import process from "process";
import { Command, Grid, Point, validateRouteInput, validatePoint } from "rover";
import Queue from "./queue";
import { routeForCommands } from "rover/src/router";

const server = express();
server.use(bodyParser.json());

server.post("/brew_coffee", (req, res, next) => {
  res.status(418).send("I'm a teapot");
  next();
});

const defaultGrid: Grid = { x: 10, y: 10 };
const defaultPosition: Point = { x: 0, y: 0, direction: "N" };

let grid: Grid = defaultGrid;
let position: Point = defaultPosition;

const queue = new Queue();

server.get("/map", (req, res, next) => {
  res.send({ grid, position });
  next();
});

server.post("/map", (req, res, next) => {
  grid = req.body?.grid ?? defaultGrid;
  position = req.body?.position ?? defaultPosition;
  queue.clear();
  res.send({ grid, position });
  next();
});

server.get("/commands", (req, res, next) => {
  const routes = queue.commands;
  res.send({ routes });
  next();
});

server.post("/commands", (req, res, next) => {
  const input = req.body?.command ?? "";
  console.log("POST commands:", req.body);
  console.log("POST commands:", req.body.command);

  const inputError = validateRouteInput(input);
  if (inputError) return sendError(res, next, inputError);

  const commands: Command[] = input.split("");
  const points = routeForCommands(commands, position);

  for (const point of points) {
    const error = validatePoint(point, grid);
    if (error) return sendError(res, next, error);
  }

  for (const command of commands) {
    queue.addCommand(command);
  }

  position = points[points.length - 1];

  res.status(201).send({ point: position });
  next();
});

if (process.env.NODE_ENV !== "test") {
  const port = process.env.SERVER_PORT ?? 8081;
  server.listen(port, () => {
    console.log(`server listening at http://localhost:${port}`);
  });
}

process.on("SIGINT", () => {
  console.info(" process interrupted ");
  process.exit(0);
});

function sendError(
  res: express.Response,
  next: express.NextFunction,
  error: string
) {
  res.status(400).send({ error });
  next();
}
