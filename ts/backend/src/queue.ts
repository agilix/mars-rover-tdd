import { Command } from "rover";
import Transport from "./transport";

export default class Queue {
  commands: Command[] = [];
  executing = false;
  transport = new Transport();

  addCommand(command: Command) {
    this.commands.push(command);
    // this.startExecution();
  }

  clear() {
    this.commands.length = 0;
  }

  startExecution() {
    if (this.executing) return;
    this.execute();
  }

  async execute() {
    const command = this.commands.shift();
    if (!command) {
      this.executing = false;
      return;
    }
    try {
      await this.transport.send(command);
    } catch (error) {
      this.commands.unshift(command);
    }
    setTimeout(() => this.execute(), 1000);
  }
}
