import { Command } from "rover";
import http from "http";

export default class Transport {
  async send(command: Command) {
    return doRequest("http://localhost:3000/" + CommandStrings[command]);
  }
}

const CommandStrings = {
  M: "move",
  L: "left",
  R: "right",
};

function doRequest(options) {
  return new Promise((resolve, reject) => {
    let req = http.request(options);

    req.on("response", (res) => {
      resolve(res);
    });

    req.on("error", (err) => {
      reject(err);
    });
  });
}
