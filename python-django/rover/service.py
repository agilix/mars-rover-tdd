from .models import GridModel, PositionModel, Queue


def new_position(x, y, direction):
    return {'point': {'x': x, 'y': y, 'direction': direction}}


class Grid:
    def __init__(self, x, y):
        self.y = y
        self.x = x


class Map:

    def init(self, grid, position):
        GridModel.objects.all().delete()
        GridModel.objects.create(
            x=grid['x'],
            y=grid['y']
        )

        PositionModel.objects.all().delete()
        PositionModel.objects.create(
            x=position['x'],
            y=position['y'],
            direction=position['direction']
        )

    def get_grid(self):
        grid = GridModel.objects.all().last()
        return Grid(grid.x, grid.x)

    def get_position(self):
        p = PositionModel.objects.all().last()
        return new_position(p.x, p.y, p.direction)

    def update_position(self, position):
        PositionModel.objects.all().delete()
        point = position['point']
        PositionModel.objects.create(x=point['x'], y=point['y'], direction=point['direction'])


class Route:
    def send_route(self, commands):
        for command in commands:
            Queue.objects.create(command=command)
