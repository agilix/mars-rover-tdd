from django.db import models


class Queue(models.Model):
    command = models.CharField(max_length=1)

    def __repr__(self):
        return f'"{self.command}" is added to queue.'

    class Meta:
        app_label = 'rover'


class GridModel(models.Model):
    x = models.IntegerField()
    y = models.IntegerField()

    def __str__(self):
        return f'Grid({self.x}, {self.y})'


class PositionModel(models.Model):
    x = models.IntegerField()
    y = models.IntegerField()
    direction = models.CharField(max_length=1)

    def __str__(self):
        return f'Position({self.x}, {self.y}, {self.direction})'
