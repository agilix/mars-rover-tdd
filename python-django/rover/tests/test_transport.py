from unittest import TestCase
from ..jobs.transport import Transport


class TransportTest(TestCase):

    def test_retry(self):
        transport = ConnectionErrorTransport(2)

        result = transport.move()

        self.assertTrue(result)

    def test_transport_retries_5_times_on_bad_request(self):
        transport = ErrorTransport(503)

        result = transport.move()

        self.assertFalse(result)

    def test_success_move(self):
        transport = AlwaysSuccessTransport()

        result = transport.move()

        self.assertTrue(result)

    def test_success_right_turn(self):
        transport = AlwaysSuccessTransport()

        result = transport.right()

        self.assertTrue(result)

    def test_success_left_turn(self):
        transport = AlwaysSuccessTransport()

        result = transport.left()

        self.assertTrue(result)


class AlwaysSuccessTransport(Transport):
    status_code = 200

    def _do(self, path):
        return self


class SilentLogger:
    def warning(self, text):
        pass

    def debug(self, text):
        pass


class ErrorTransport(Transport):
    logger = SilentLogger()

    def __init__(self, error_code):
        self.status_code = error_code

    def _do(self, path):
        return self

    def _delay(self, duration):
        pass


class ConnectionErrorTransport(Transport):
    logger = SilentLogger()
    status_code = 200

    def __init__(self, num):
        self.num = num

    def _do(self, path):
        if self.num > 0:
            self.num -= 1
            raise ConnectionError

        return self
