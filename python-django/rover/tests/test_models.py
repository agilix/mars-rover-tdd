from django.test import TestCase

from ..models import Queue


class QueueTest(TestCase):

    def test_command_to_queue(self):
        Queue.objects.create(command='M')

        result = Queue.objects.all()

        self.assertEqual(len(list(result)), 1)
