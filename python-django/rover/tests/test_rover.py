from dataclasses import dataclass

from django.test import TestCase
from ..service import new_position, Grid
from ..views import NonExistingDirection, IncorrectMove, Rover, Position
from ..jobs.transport import Transport


class TestableTransport(Transport):

    def move(self):
        pass


class MoveTest(TestCase):

    def test_rover_cannot_move_outside_grid(self):

        @dataclass
        class RoverTestCase:
            name: str
            input: Position

        testcases = [
            RoverTestCase(name="top bound", input=new_position(0, 1, 'N')),
            RoverTestCase(name="bottom bound", input=new_position(0, 0, 'S')),
            RoverTestCase(name="left bound", input=new_position(0, 0, 'W')),
            RoverTestCase(name="right bound", input=new_position(1, 0, 'E')),
        ]
        for case in testcases:
            rover = Rover(Grid(2, 2), case.input)
            with self.assertRaises(IncorrectMove):
                rover.move()

    def test_make_one_step(self):
        position = {'point': {'x': 0, 'y': 0, 'direction': 'N'}}
        rover = Rover(Grid(10, 10), position)

        result = rover.make_action('M')

        self.assertEqual(result['point']['x'], 0)
        self.assertEqual(result['point']['y'], 1)

    def test_make_two_steps(self):
        position = {'point': {'x': 0, 'y': 0, 'direction': 'N'}}
        rover = Rover(Grid(10, 10), position)
        rover.make_action('M')

        result = rover.make_action('M')

        self.assertEqual(result['point']['x'], 0)
        self.assertEqual(result['point']['y'], 2)

    def test_turn_left(self):
        @dataclass
        class RoverTestCase:
            name: str
            input: Position
            expected: Position

        testcases = [
            RoverTestCase(name="S -> E", input=new_position(0, 0, 'S'), expected=new_position(0, 0, 'E')),
            RoverTestCase(name="E -> N", input=new_position(0, 0, 'E'), expected=new_position(0, 0, 'N')),
            RoverTestCase(name="N -> W", input=new_position(0, 0, 'N'), expected=new_position(0, 0, 'W')),
            RoverTestCase(name="W -> S", input=new_position(0, 0, 'W'), expected=new_position(0, 0, 'S')),
        ]

        for case in testcases:
            rover = Rover(Grid(10, 10), case.input)

            rover.rotate_left()

            self.assertEqual(rover.position, case.expected)

    def test_turn_right(self):
        @dataclass
        class RoverTestCase:
            name: str
            input: Position
            expected: Position

        testcases = [
            RoverTestCase(name="N -> E", input=new_position(0, 0, 'N'), expected=new_position(0, 0, 'E')),
            RoverTestCase(name="E -> S", input=new_position(0, 0, 'E'), expected=new_position(0, 0, 'S')),
            RoverTestCase(name="S -> W", input=new_position(0, 0, 'S'), expected=new_position(0, 0, 'W')),
            RoverTestCase(name="W -> N", input=new_position(0, 0, 'W'), expected=new_position(0, 0, 'N')),
        ]

        for case in testcases:
            rover = Rover(Grid(10, 10), case.input)

            rover.rotate_right()

            self.assertEqual(rover.position, case.expected)

    def test_move(self):
        @dataclass
        class RoverTestCase:
            name: str
            input: Position
            expected: Position

        testcases = [
            RoverTestCase(name="to North", input=new_position(0, 0, 'N'), expected=new_position(0, 1, 'N')),
            RoverTestCase(name="to East", input=new_position(0, 0, 'E'), expected=new_position(1, 0, 'E')),
            RoverTestCase(name="to South", input=new_position(0, 1, 'S'), expected=new_position(0, 0, 'S')),
            RoverTestCase(name="to West", input=new_position(1, 0, 'W'), expected=new_position(0, 0, 'W')),
        ]

        for case in testcases:
            rover = Rover(Grid(10, 10), case.input)

            rover.move()

            self.assertEqual(rover.position, case.expected)

    def test_non_existing_direction(self):
        rover = Rover(None, new_position(0, 0, 'Z'))

        with self.assertRaises(NonExistingDirection):
            rover.rotate_left()
