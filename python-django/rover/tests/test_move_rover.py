from unittest import TestCase

from ..jobs.move_rover import Rover
from ..jobs.transport import Transport


class FakeTransport(Transport):
    move_called = False

    def move(self):
        self.move_called = True
        return True


class MoveRoverTest(TestCase):

    def test_on_M_command_calls_transport_move(self):
        transport = FakeTransport()
        rover = Rover(transport)

        rover.send('M')

        self.assertTrue(transport.move_called)
