import json

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from ..models import GridModel, PositionModel


class CreatePreviewTests(TestCase):
    def setUp(self) -> None:
        GridModel.objects.all().delete()
        GridModel.objects.create(x=10, y=10)
        PositionModel.objects.all().delete()
        PositionModel.objects.create(x=0, y=0, direction='N')

    def test_make_one_step_on_north(self):
        response = self.client.post(
            reverse('commands'),
            data={'command': 'M'},
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        body = json.loads(response.content)
        self.assertEqual(body['point']['x'], 0)
        self.assertEqual(body['point']['y'], 1)
        self.assertEqual(body['point']['direction'], 'N')

    def test_make_two_steps(self):
        response = self.client.post(
            reverse('commands'),
            data={'command': 'MM'},
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        body = json.loads(response.content)
        self.assertEqual(body['point']['x'], 0)
        self.assertEqual(body['point']['y'], 2)
        self.assertEqual(body['point']['direction'], 'N')

    def test_set_map(self):
        response = self.client.post(
            reverse('map'),
            data={'position': {'x': 0, 'y': 0, 'direction': "N"}, 'grid': {'x': 10, 'y': 10}},
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
