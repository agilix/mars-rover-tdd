import logging
import time

import requests


class Transport:
    logger = logging.getLogger(__name__)

    def move(self):
        return self._send("move")

    def right(self):
        return self._send("right")

    def left(self):
        return self._send("left")

    def _send(self, path):
        retries = 5
        result = None
        while retries > 0:
            try:
                result = self._do(path)
                break
            except Exception as e:
                self.logger.warning("error: " + str(e) + ", retries left: " + str(retries))
                retries -= 1
                continue

        while retries > 0:
            if result.status_code != 200:
                self.logger.warning(f'http error: {result.status_code},  retries left: {retries} ')
                result = self._do(path)
                retries -= 1
                self._delay(1)
                continue
            else:
                break

        return result.status_code == 200

    def _do(self, path):
        return requests.get('http://localhost:3000/' + path)

    def _delay(self, duration):
        time.sleep(duration)