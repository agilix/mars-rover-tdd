import logging
import time

from django_extensions.management.jobs import BaseJob

from ..models import Queue
from .transport import Transport


class UnknownCommand(Exception):
    pass


class Rover:
    def __init__(self, transport):
        self.transport = transport

    def send(self, next_command):
        try:
            print(f'Command: {next_command}')
            if next_command == 'M':
                self.transport.move()
            elif next_command == 'R':
                self.transport.right()
            elif next_command == 'L':
                self.transport.left()
            else:
                raise UnknownCommand(next_command)

        except Exception as e:
            print(f'Error: {e}')
            return

        print("OK!")


class Job(BaseJob):
    help = "Queue job."
    logger = logging.getLogger(__name__)
    rover = Rover(Transport())

    def execute(self):
        while True:
            next_command = self.get_next_command()
            if self.rover.send(next_command.command):
                self.mark_command_complete(next_command)
            time.sleep(1)

    def get_next_command(self):
        return Queue.objects.order_by('id')[0:1].get()

    def mark_command_complete(self, command):
        command.delete()
