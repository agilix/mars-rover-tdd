import logging

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.views import APIView

from .service import Map, Route

Point = dict[str, str]
Position = dict[str, Point]


class Rover:

    def __init__(self, grid, position):
        self.position = position
        self.grid = grid

    def make_action(self, command) -> None:

        if command == 'M':
            return self.move()

        if command == 'R':
            return self.rotate_right()

        if command == 'L':
            return self.rotate_left()

        raise UnknownCommand(command)

    def rotate_right(self):

        if self.position['point']['direction'] == 'N':
            self.position['point']['direction'] = 'E'
        elif self.position['point']['direction'] == 'E':
            self.position['point']['direction'] = 'S'
        elif self.position['point']['direction'] == 'S':
            self.position['point']['direction'] = 'W'
        elif self.position['point']['direction'] == 'W':
            self.position['point']['direction'] = 'N'
        else:
            raise NonExistingDirection(self.position['point']['direction'])

        return self.position

    def rotate_left(self):
        position = self.position
        if position['point']['direction'] == 'S':
            position['point']['direction'] = 'E'
        elif position['point']['direction'] == 'E':
            position['point']['direction'] = 'N'
        elif position['point']['direction'] == 'N':
            position['point']['direction'] = 'W'
        elif position['point']['direction'] == 'W':
            position['point']['direction'] = 'S'
        else:
            raise NonExistingDirection(position['point']['direction'])

        return position

    def move(self):

        position = self.position

        if position['point']['direction'] == 'N':
            position['point']['y'] += 1
        elif position['point']['direction'] == 'S':
            position['point']['y'] -= 1
        elif position['point']['direction'] == 'E':
            position['point']['x'] += 1
        elif position['point']['direction'] == 'W':
            position['point']['x'] -= 1
        else:
            raise NonExistingDirection(position['point']['direction'])

        self.validate_position(position['point'])

        return position

    def validate_position(self, point):
        if point['y'] >= self.grid.y:
            raise IncorrectMove('top boundary')
        if point['y'] < 0:
            raise IncorrectMove('down boundary')
        if point['x'] < 0:
            raise IncorrectMove('left boundary')
        if point['x'] >= self.grid.x:
            raise IncorrectMove('right boundary')


class NonExistingDirection(Exception):
    pass


class UnknownCommand(Exception):
    pass


class IncorrectMove(Exception):
    pass


class Coffee(APIView):
    @csrf_exempt
    def brew(self):
        return HttpResponse("I'm a teapot", status=status.HTTP_418_IM_A_TEAPOT)


class MapView(APIView):
    logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.map = Map()

    def post(self, request):
        grid = request.data['grid']
        position = request.data['position']
        self.map.init(grid, position)
        return JsonResponse({'status': 'OK'}, status=status.HTTP_201_CREATED)


class Commands(APIView):
    logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.map = Map()
        self.route = Route()

    def post(self, request):
        commands = list(request.data['command'])
        self.logger.warning(f'received: {commands}')
        try:
            rover = Rover(self.map.get_grid(), self.map.get_position())
            for command in commands:
                rover.make_action(command)
                self.map.update_position(rover.position)

            self.route.send_route(commands)

            position = rover.position
        except IncorrectMove as e:
            self.logger.warning(f'error: {e}')
            return JsonResponse({'error': 'unable to cross grid boundary'}, status=status.HTTP_400_BAD_REQUEST)

        return JsonResponse(position, status=status.HTTP_201_CREATED)
