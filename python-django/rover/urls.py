from django.urls import path

from . import views

urlpatterns = [
    path('brew_coffee', views.Coffee.brew),
    path('commands', views.Commands.as_view(), name='commands'),
    path('map', views.MapView.as_view(), name='map'),
]
